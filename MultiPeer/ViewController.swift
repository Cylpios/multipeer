//
//  ViewController.swift
//  MultiPeer
//
//  Created by gouvaert johnny on 12/10/2017.
//  Copyright © 2017 Gouvaert. All rights reserved.
//

import UIKit
import MultipeerConnectivity
//Le multipeer connect fonctionne avec un advertiser adv, un researcher RS

class ViewController: UIViewController {
    
    //Etat du Advertiser
    @IBOutlet weak var titleBar: UINavigationItem!
    var isAdv : Bool!
    
    //Tableau
    @IBOutlet weak var tblPeers: UITableView!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        tblPeers.delegate = self as? UITableViewDelegate
        tblPeers.dataSource = self as? UITableViewDataSource
        
        appDelegate.mpcManager.delegate = self as? MPCManagerDelegate
        
        appDelegate.mpcManager.browser.startBrowsingForPeers()
        
        appDelegate.mpcManager.advertiser.startAdvertisingPeer()
        
        isAdv = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func advChange(_ sender: UISwitch) {
        let actionSheet = UIAlertController(title: "Changement de votre visibilité", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        
        let visibilityAction: UIAlertAction = UIAlertAction(title: "Valider", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            if self.isAdv == true {
                self.appDelegate.mpcManager.advertiser.stopAdvertisingPeer()
            }
            else{
                self.appDelegate.mpcManager.advertiser.startAdvertisingPeer()
            }
            
            self.isAdv = !self.isAdv
        }
        
        //Ajout d'une fonction annuler, au cas ou l'utilisateur appui par megarde
        let cancelAction = UIAlertAction(title: "Annuler", style: UIAlertActionStyle.cancel) { (alertAction) -> Void in
            sender.setOn(true, animated: true) // <=> sender.setOn(!sender.isOn, animated: true)
        }
        
        actionSheet.addAction(visibilityAction)
        actionSheet.addAction(cancelAction)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    //On ajoute les fonctions définies dans MCPManager
    
    func foundPeer() {tblPeers.reloadData()}
    
    func lostPeer(){tblPeers.reloadData()}

    func invitationWasReceived(fromPeer: String) {
        //Initialisation de la boite de dialogue
        let alert = UIAlertController(title: "\(fromPeer)", message:"cherche a vous joindre", preferredStyle: UIAlertControllerStyle.alert)
        
        //Initialisation d'une reponse, Accepter
        let acceptAction: UIAlertAction = UIAlertAction(title: "Accepter", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.appDelegate.mpcManager.invitationHandler(true, self.appDelegate.mpcManager.session)
        }
        
        //Initialisation d'une reponse, Refuser
        let declineAction = UIAlertAction(title: "Refuser", style: UIAlertActionStyle.cancel) { (alertAction) -> Void in
            self.appDelegate.mpcManager.invitationHandler(false, nil)
        }
        
        //Ajout des reponses a la boite de dialogue
        alert.addAction(acceptAction)
        alert.addAction(declineAction)
        
        //On ajoute dans la liste des operations a executer, l'affichage de notre boite de dialogue
        OperationQueue.main.addOperation { () -> Void in
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func connecterWithPeer(peerID: MCPeerID){
        OperationQueue.main.addOperation {
            () -> Void in self.performSegue(withIdentifier: "idSegueChat", sender: self)
        }
    }    
}

