//
//  ChatViewController.swift
//  MultiPeer
//
//  Created by gouvaert johnny on 22/10/2017.
//  Copyright � 2017 Gouvaert. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class ChatViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var titleBar: UINavigationItem!
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var txtChat: UITextField!
    
    @IBOutlet weak var tblChat: UITableView!
    
    var messagesArray: [Dictionary<String, String>] = []
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblChat.delegate = self
        tblChat.dataSource = self
        
        tblChat.estimatedRowHeight = 60.0
        tblChat.rowHeight = UITableViewAutomaticDimension
        
        txtChat.delegate = self
        
        titleBar.title = String(describing: appDelegate.mpcManager.session.connectedPeers[0] as MCPeerID)
        
        NotificationCenter.default.addObserver(self, selector: Selector("handleMPCReceivedDataWithNotification:"), name: NSNotification.Name(rawValue: "receivedMPCDataNotification"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func endchat(_ sender: UIBarButtonItem) {
        let messageDictionary: [String: String] = ["message": "_end_chat_"]
        if appDelegate.mpcManager.sendData(dictionaryWithData: messageDictionary, toPeer: appDelegate.mpcManager.session.connectedPeers[0] as MCPeerID){
            self.dismiss(animated: true, completion: { () -> Void in
                self.appDelegate.mpcManager.session.disconnect()
            })
        }
    }
    
    //Gestion de la UITableView
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messagesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "idCell") as! UITableViewCell
        
        let currentMessage = messagesArray[indexPath.row] as Dictionary<String, String>
        
        if let sender = currentMessage["sender"] {
            var senderLabelText: String
            var senderColor: UIColor
            
            if sender == "self"{
                senderLabelText = "Moi :"
                senderColor = UIColor.lightGray
            }
            else{
                senderLabelText = sender + ":"
                senderColor = UIColor.black
            }
            
            cell.detailTextLabel?.text = senderLabelText
            cell.detailTextLabel?.textColor = senderColor
        }
        
        if let message = currentMessage["message"] {
            cell.textLabel?.text = message
        }
        
        return cell
    }
    
    //Ajout du UITextFieldDelegate, envoi de message
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        let messageDictionary: [String: String] = ["message": textField.text!]
        
        if appDelegate.mpcManager.sendData(dictionaryWithData: messageDictionary, toPeer: appDelegate.mpcManager.session.connectedPeers[0] as MCPeerID){
            
            var dictionary: [String: String] = ["sender": "self", "message": textField.text!]
            messagesArray.append(dictionary)
            
            self.updateTableview()
        }
        else{
            print("Impossible d'envoyer le message")
        }
        
        textField.text = ""
        
        return true
    }
    
    @IBAction func sendchat(_ sender: UIButton) {
        
        
    }
    
    //Autres
    //Mise a jours du tableau
    func updateTableview(){
        tblChat.reloadData()
        
        if self.tblChat.contentSize.height > self.tblChat.frame.size.height {
            tblChat.scrollToRow(at: IndexPath(row: messagesArray.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
        }
        /////////A verifier !!
    }
    
    //Gestion de la reception de message
    func handleMPCReceivedDataWithNotification(notification: NSNotification) {
        // Dictionnaire contenant les informations recues de les sources PEER.
        let receivedDataDictionary = notification.object as! Dictionary<String, AnyObject>
        
        // On separe les informations et les sources.
        let data = receivedDataDictionary["data"] as? NSData
        let fromPeer = receivedDataDictionary["fromPeer"] as! MCPeerID
        
        // On convertit les informations dans un dictionnaire "Object" pour pouvoir les parcourir plus facilement.
        let dataDictionary = NSKeyedUnarchiver.unarchiveObject(with: data! as Data) as! Dictionary<String, String>
        
        // On cherche le debut du message, grace a la cle "message".
        if let message = dataDictionary["message"] {
            // On verifie que le message n'est pas une demande d'arret du chat, grace a la cle "_end_chat_".
            if message != "_end_chat_"{
                // On creait un nouveau dictionnaire contenant les informations sur les noeuds du PEER
                let messageDictionary: [String: String] = ["sender": fromPeer.displayName, "message": message]
                
                // On ajoute ces informations au "messagesArray"
                messagesArray.append(messageDictionary)
                
                // On recharge les informations (Ce qui a aussi pour effet de ramener l'utilisateur en bas de la page)
                OperationQueue.main.addOperation({ () -> Void in
                    self.updateTableview()
                })
            }
            else{
                // Dans le cas d'un "_end_chat_", soit arret du flux de message.
                // On previens l'utilisateur.
                let alert = UIAlertController(title: "", message: "\(fromPeer.displayName) a quitt� la conversation.", preferredStyle: UIAlertControllerStyle.alert)

                let doneAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (alertAction) -> Void in
                    self.appDelegate.mpcManager.session.disconnect()
                    self.dismiss(animated: true, completion: nil)
                }
                
                alert.addAction(doneAction)
                
                OperationQueue.main.addOperation({ () -> Void in
                    self.present(alert, animated: true, completion: nil)
                })
            }
        }
    }
}
