//
//  RoundButton.swift
//  MultiPeer
//
//  Created by Johnny on 30/10/2017.
//  Copyright © 2017 Gouvaert. All rights reserved.
//

import UIKit
@IBDesignable
class RoundButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateCornerRadius()
    }
    
    @IBInspectable var rounded: Bool = false {
        didSet {
            updateCornerRadius()
        }
    }
    
    func updateCornerRadius() {
        layer.cornerRadius = rounded ? frame.size.height / 2 : 0
    }
}
