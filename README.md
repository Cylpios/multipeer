# MultiPeer messaging App by Gouvaert Johnny
 Codé avec Xcode Version 9.0.1 (9A1004)

# *Version 0.2.2*

###  Ajouts :
* Amelioration de l'interface
* Ajout de l'écran de chargement

### À faire :
* Envoi Multimédia 
    * de photo
    * de musique
    - (De façon à ce que le transfert soit le plus rapide possible)
* Connection entre plus de 2 Peer
    - (Comment ??)

## Contient :
* Ecran de chargement et Interface améliorée
* Gestion de peer (Connexion entre deux peers uniquement) 
* Envoi et affichage de message

## Technologies Utilisées :
* [MultipeerConnectivity]
---
Merci au site AppCoda
En cas de problèmes contacter, Gouvaert Johnny

[MultipeerConnectivity]: <https://developer.apple.com/documentation/multipeerconnectivity>